# -*- coding: utf-8 -*-
"""
    qst
    ~~~

    Provides the ``qst`` directive

    Usage::

        .. qst:: Question

           Content

    The argument for ``qst`` is a question

    :copyright: Copyright 2007-2011 by the IRI team.
    :license: BSD, see LICENSE for details.
"""

from docutils import nodes

from sphinx.util.nodes import set_source_info
from sphinx.util.compat import Directive


class qst(nodes.Element): pass


class Question(Directive):

    has_content = True
    required_arguments = 1
    optional_arguments = 0
    final_argument_whitespace = True
    option_spec = {}

    def run(self):
        node = qst()
        node.document = self.state.document
        set_source_info(self, node)
        node['qst'] = self.arguments[0]
        self.state.nested_parse(self.content, self.content_offset,
                                node, match_titles=1)
        return [node]


def process_qst_nodes(app, doctree, docname):
    ns = app.config.__dict__.copy()
    ns['builder'] = app.builder.name
    for node in doctree.traverse(qst):
       node.replace_self(node.children)


def setup(app):
    app.add_node(qst)
    app.add_directive('qst', Question)
    app.connect('doctree-resolved', process_qst_nodes)
